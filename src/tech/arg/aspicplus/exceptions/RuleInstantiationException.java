/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tech.arg.aspicplus.exceptions;

/**
 *
 * @author mark
 */
public class RuleInstantiationException extends Exception {

    public RuleInstantiationException(String msg){
        super(msg);
    }
}
